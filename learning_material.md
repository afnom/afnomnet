---
layout: home
title: Learn
permalink: /learn/
---

## Tools

**Network**
: [Wireshark](https://www.wireshark.org/){:target="_new"} & [pyShark](https://pypi.org/project/pyshark/){:target="_new"}
: [Aircrack-ng](https://www.aircrack-ng.org/){:target="_new"}

**Disassemblers**
: [Ghidra](https://ghidra-sre.org/){:target="_new"}
: [Cutter](https://cutter.re/){:target="_new"} (comes with [r2](https://rada.re/n/radare2.html){:target="_new"} and [Ghidra](https://ghidra-sre.org/){:target="_new"})
: [IDA Pro v7 Free](https://www.hex-rays.com/products/ida/support/download_freeware.shtml){:target="_new"} ([cheatsheet](https://www.hex-rays.com/products/ida/support/freefiles/IDA_Pro_Shortcuts.pdf){:target="_new"})
: [JD GUI](http://jd.benow.ca){:target="_new"}
: [OllyBbg](http://www.ollydbg.de){:target="_new"}
: [Hopper](https://www.hopperapp.com/){:target="_new"}
: [Uncompyle2](https://github.com/Mysterie/uncompyle2){:target="_new"} (for python)

**Editors**
: [0xED](https://www.suavetech.com/0xed/){:target="_new"} (for macos)

**Proxies**
: [Burp Proxy](https://portswigger.net/burp/proxy.html){:target="_new"}
: [ZAP](https://owasp.org/www-project-zap/){:target="_new"}

**Debuggers**
: [GDB](https://www.gnu.org/software/gdb){:target="_new"} <i class="fas fa-heart"></i>

## Learning material

**The best place to get started with _any_ hacking is [WarGames by OverTheWire](https://www.overthewire.org/wargames){:target="_new"}. The best first CTF practice is [picoCTF](https://picoctf.com/){:target="_new"}.** 

**Hacking Platforms**
: [HackTheBox](https://www.hackthebox.eu/){:target="_new"}
: [Immersive Labs](https://dca.immersivelabs.online/signin){:target="_new"} (free for students)
: [TryHackMe](https://tryhackme.com/){:target="_new"}

**Linux**
: [Commandline Challenge](https://cmdchallenge.com/){:target="_new"}

**Web**
: [Damn Vulnerable Web App](http://www.dvwa.co.uk){:target="_new"}
: [Google Gruyere](https://google-gruyere.appspot.com){:target="_new"}
: [HackThisSite](https://www.hackthissite.org){:target="_new"}
: [EnigmaGroup](https://www.enigmagroup.org){:target="_new"}
: [Pentesters lab](https://www.pentesterlab.com){:target="_new"}
: [Pentestmonkey](http://pentestmonkey.net/){:target="_new"}

**Crypto**
: [CryptoHack](https://cryptohack.org/){:target="_new"}

**Pwn/RE**
: [Exploit Exercises](http://exploit-exercises.com){:target="_new"}
: [Smash the Stack](http://smashthestack.org){:target="_new"}
: [pwnable.kr](http://pwnable.kr/){:target="_new"}
: [Azeria Labs](https://azeria-labs.com/){:target="_new"} (ARM)

**Mixed**
: [w3challs](http://w3challs.com/){:target="_new"}
: [VulnHub](https://vulnhub.com){:target="_new"}
: [Hellbound Hackers](https://hellboundhackers.org){:target="_new"}
: [CTF Practice](http://captf.com/practice-ctf/){:target="_new"}
: [Metasploit Unleashed](https://www.offensive-security.com/metasploit-unleashed/){:target="_new"}

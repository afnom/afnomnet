from hashlib import sha256
from json import loads
from string import printable

flag = ""

with open("accessLog.json") as accessLog:
    JSON = loads(accessLog.read())

for entry in JSON['data']['accessLog']:
    hash_ = loads(entry['args']).get("hash", None)
    if hash_ is None:
        continue
    for char in printable:
        if hash_ == sha256((flag + char).encode()).hexdigest():
            flag += char
            print(char, end='', flush=True)
            break
print()

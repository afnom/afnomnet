---
layout: post
title: "Welcome back to 2020/21!"
author: leet_lemon
---

Hey all! Hopefully you're all staying safe wherever you are, and looking
forward to a new year at uni! Despite unforseen circumstances (mumble, global
pandemic, mumble mumble), we've continued meeting online through the summer,
something we wouldn't normally have done! We've taken part in CTFs, kept
going at HackTheBox, and even built a few challenges of our own.

It's taken some getting used to, but we've continued taking part in our
regular weekly hacking sessions, followed by a bit of pub, each in our own
houses 🍻 It's been a really good opportunity to connect with members old and
new, and get the chance to have more spontaneous events, when we're all free!

Not only that, but we've been involved in the EPS LOCKDONE events over the
past few weeks or so and helped run a super beginner friendly Capture the
Flag competition as part of that! We had more participants than expected, and
got the chance to introduce more people to the world of ethical hacking 🎉

We're all really looking forward to this new academic year, and while we're
still working out all the details, we're still planning to run the same
events we've always done, taking everything online, while still making sure
to keep our friendly chill atmosphere (and beloved pub). We're kicking things
off on the 30th of September, with our annual Linux installfest - it's gonna
be a *little* different, but we're gonna be providing documentation and
support for anyone wanting to get setup with Linux, an essentially tool for
ethical hackers like us :)

From then out, we're gonna be meeting every Wednesday 15:00-17:00! So make
you come along and join us on our discord at <https://discord.gg/hhtruqR>!

---
layout: post
title: "Lightning Talks Oct 2020"
author: neko3
---

What a busy start of term we've had so far! InstallFest done, our first web
hacking sessions successfully completed, and hosting our first event (in what
could become a series!) of lightning talks! Credit goes to
[leet_lemon &#127819;](https://twitter.com/jedevc), as the primary driver behind this
event ! 

The idea behind it is simple: *six different 10 minutes talks, on anything!* 
As a speaker, 10 minutes is a sweet spot of getting your passion across,
but not too long that it takes too much time to prepare for! As an attendee, 
the talks are short enough that you don't have time to get bored! &#129395;

And what a great time we had! The toughest part of it all was choosing the
talks, as we had such an array of **amazing** talks submitted &#128562;! We
would have loved to hear them all but alas, we could only do six. We hope to
see the rest of them in the near future though!

Our final talks schedule was:
- @heavyimage - [LD_PRELOAD: changing random number generators to always return
  4]({{ "/files/talks/LD_preload.pdf" | base_url }}){:target="_new"}
- @raenlok - [CRT Monitors - Why you should be playing all your games on 20 year
  old hardware](https://docs.google.com/presentation/d/1jqAag-YiPFAaGTUbErkBYr1yasulSdrgdgSXlz_ES9g/edit?usp=sharing){:target="_new"}
- @h33p - [Hacking systems through Direct Memory Access
  (DMA)](https://h33p.github.io/dma_talks/2020_10_AFNOM_Lightning/slides.html){:target="_new"}
- @neko3 - [Relaying contactless payment cards across a room... or the
  ocean!](https://neko3.gitlab.io/pres/emv-pres){:target="_new"}
- @BladorthinGrey - [Why has the English Language lost letters from its alphabet
  and should we replace them?]({{ "/files/talks/Lost_Letters.pdf" | base_url }}){:target="_new"}
- @dxf - [Vulnerabilities by manipulating Deep Learning]({{ "/files/talks/Hacking_computer_brains.pdf" | base_url }}){:target="_new"}

We were over the moon with the range of talks we had, and we loved every minute
of the event! We were so happy to see our (over 50!) attendees were engaged and
had brilliant questions for the speakers. 

We learnt a lot about fair conference-style selection processes, and about 
the technical side of running such an event. 

We would like thank everyone involved, from speakers, to mentors and to our
awesome attendees! We're looking forward to hosting another set of talks
soon! &#128155;

---
layout: post
title: "Linux InstallFest 2020"
author: neko3
---

The new academic year is once again upon us, and that means it is time for the
Linux InstallFest! Every year, we help anyone interested in starting their
journey into the world of Linux to get a working setup on their machine.

This year, as we went all virtual, we had a lot of work to do! We never
debugged Linux installs over the internet, how would that go? Thankfully, 
it was all a breeze in the end. 

We prepared a thorough [guide](https://linux.afnom.net) on installing Linux
(Ubuntu or Manjaro) either as replacement OS, dual boot or in a VM.
We added a section on getting some useful software up and running, as well
as how to get a working Java and Eclipse setup, especially useful for our
first year students.

We setup our [Discord](https://afnom.net/discord) to have pairs of voice
channels and text channels for every category of install we supported and got
the help of many amazing volunteers to guide everyone through their installs.

We finished it all off with a nice social in virtual Bratby bar &#127865; (for those who
don't know, the Bratby bar on campus is the place where we had our socials,
after a hacking session -- it makes sense we carry on the tradition!).

For further troubleshooting, [CSS](https://cssbham.com/discord) have kindly 
setup a `#tech-support` channel on their Discord server, where members
of the community can help each other &#129309;.

We think it was a very successful event, and we would like to thank all of 
those who helped contribute! Spreading the love of Free and Open Source 
Software every year! &#128147;


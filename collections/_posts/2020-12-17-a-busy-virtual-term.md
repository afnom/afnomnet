---
layout: post
title: "A busy virtual term!"
author: leet_lemon
---

Hey all! It's probably not a surprise that this has been one of our most
chaotic terms ever! Thankfully, we've been able to keep going with our regular
meetings, just like always, even though we've moved online.

We kicked off the term with our Linux Installfest, using our Discord server to
help manage all the different setups that everyone wanted. After that, we went
through an introduction to hacking, understanding Linux, and attacking
websites.  Of course, we've continued our virtual pub trips, learning and
playing various party games, such as Codenames, Wavelength and even Geoguessr
online!

We've also continued taking part in our signature Capture The Flag (CTF)
competitions, though it's been trickier than ever to find the time to fit them
in! We took part in the HackTheBox Uni CTF, coming 16th out of 180 university
teams and 2nd out of all UK teams, netting us a place in the upcoming finals
which we're all looking forward to! We've also taken part in D-CTF again, and
we're hoping to have a go at another one over the Christmas break at some point
:)

Just this last Wednesday we held our last session of this term, our Christmas
session, crowdsourcing all the challenges from within our own member base. We
hacked Santa's password manager, ran programs disguised as a Christmas Carol
and investigated a terrifying web server running bizarre software. Yes, it was
actually about as bizarre as it sounds.

Next term, we're looking forward to continuing our regular sessions, hopefully
doing incredibly well in the HackTheBox finals, and running our very own
WhatTheCTF competition again for the 3rd time!

Merry Christmas to you all, and have a happy new year!

![Discord Event AFNOM]({{ "files/photos/virtual-events-dec20.jpg" | relative_url }})

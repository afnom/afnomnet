---
layout: page
title: Home
---

<div class="grid-header-custom">
  <div class="grid-item-header">
    <img class="logo" src="{{ "/files/afnom_logo.png" | relative_url }}" alt="AFNOM Logo" title="AFNOM Logo" />
  </div>

  <div class="grid-item-header">
    <h1 class="right-text">AFNOM</h1>
    <h3 class="right-text">University of Birmingham's <br /> Monkey-Inspired Hacking Club</h3>
  </div>
</div>

## Who We Are

We are the Hacking Club at the University of Birmingham. We get together
every week to look at offensive hacking and cyber security. We do this
because understanding threats and how attackers think is the best way to keep
computers secure... also it's fun.

> Any problem can be solved by enough monkeys acting completely at random.

## Meetings

All students are welcome, we can find hacking challenges that are appropriate
for every skill level, however you must bring a laptop.

We meet every Wednesday, in person, from 3pm. We announce the room on Discord, so please join with the link below. You can also drop us an email at
<a href='ma&#105;lto&#58;&#99;h&#37;61%6Fs&#64;&#97;%66&#37;6&#69;o&#109;&#46;%6E%65t'>ch&#97;o&#115;&#64;af&#110;&#111;m&#46;net</a>.
Check the calendar below, it mentions all our events, sessions topics and CTFs we're attending!
(if you cannot see anything in the calendar, check your ad blocker)

All members/attendees/visitors agree to follow the [Code of Conduct]({{
"conduct" | relative_url }}) when attending any of our events, either in-person
or online.

## Contact

<i class="fab fa-discord"></i> Discord: [AFNOM Discord](https://discord.gg/JpZ2CgX){:target="_new"}  
<i class="fas fa-envelope"></i> Email: <a href='ma&#105;lto&#58;&#99;h&#37;61%6Fs&#64;&#97;%66&#37;6&#69;o&#109;&#46;%6E%65t'>ch&#97;o&#115;&#64;af&#110;&#111;m&#46;net</a>  
<i class="fab fa-twitter"></i> Twitter: <a href="https://twitter.com/uob_afnom">@UoB_AFNOM</a>

## Schedule

Below you can find info on what we will be doing in the upcoming sessions.
Important CTFs in which we will participate are also mentioned.

<div id='loading'>Loading...</div>
<div id='calendar'></div>

## Publications

We're passionate about creating better learning environments for students!
Some of our members have written about this and shared their work with the world.
Check them out below:

[Learn-Apply-Reinforce/Share Learning: Hackathons and CTFs as General Pedagogic Tools in Higher Education, and Their Applicability to Distance Learning](https://arxiv.org/pdf/2006.04226.pdf)  
Tom Goodman and Andreea-Ina Radu  
Under review 

[Organising Monkeys or How to Run a Hacking Club]({{"files/vibrant/Organising_Monkeys.pdf" | relative_url }})  
Andreea-Ina Radu and Sam L. Thomas  
Vibrant Workshop 2015, The first UK Workshop on Cybersecurity Training & Education  
[Presentation slides]({{"files/vibrant/Organising_monkeys_pres.pdf" | relative_url}})



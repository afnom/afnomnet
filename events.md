---
layout: home
title: Events
permalink: /events/
---

# Events

Here you can find a list of events we have run or we are planning to
host in the near future.

## WhatTheCTF?!

<div class="grid-ctfs-custom">
  {% for ctf in site.wtctf %}
    {% if ctf.category == "index" %}
      <div class="grid-item-ctf">
        <h3>{{ ctf.title }} </h3>
        <p> {{ ctf.desc }}</p>
        <p>
          {% assign filename = ctf.logo | prepend: "/files/" %}
          <a
            href="{{ ctf.url | relative_url }}"
            class="ctf"
            target="_new"
          >
            <img src='{{ filename | relative_url}}' style="height: 10em;" />
          </a>
        </p>
      </div>
    {% endif %}
  {% endfor %}
</div>

## Internal

<div class="grid-ctfs-custom">
  <div class="grid-item-ctf">
  <h3> Linux InstallFest</h3>
      
  <img src='{{ "files/installfest.png" | relative_url}}' style="height: 10em;" />
  
  <p>We have been running a yearly Linux InstallFest since 2018. During this
  event we help anyone who is interested in setting up Linux as their
  Operating System go through the process, and help debug any potential
  issues that arise. We do this because, on the one hand, Linux is the
  best OS for ethical hacking, and many of the tools we use in our sessions
  work best on Linux. But also because we want to share the love for Free
  Open Source Software! This is our little contribution.</p>
  </div>
      
  <div class="grid-item-ctf">
<h3> Lightning Talks</h3>
      
  <img src='{{ "files/lightning-talks.png" | relative_url}}' style="height: 10em;" />
  
  <p>A new series of events, the Lightning Talks are a great opportunity
  for anyone who wants to share something they are passionate about with
  our community! Ran as a session of six 10-minute talks with a relaxed
  Q&A at the end and a social afterwards at our great (and virtual) Bratby
  bar! Checkout out <a href='{{ "/blog" | base_url }}'>blog</a> for
  summaries of our Lightning talks events!</p>
  </div>
</div>
 


## External

<div class="grid-ctfs-custom">
  {% for ctf in site.ctfs reversed %}
    {% if ctf.category == "index" %}
      <div class="grid-item-ctf">
        <h3>{{ ctf.title }}</h3>
        <p>{{ ctf.desc }}</p>
        <p>
          {% assign filename = ctf.logo | prepend: "/files/" %}
          <a
            href="{{ ctf.url | relative_url }}"
            class="ctf"
            target="_new"
          >
            <img src='{{ filename | relative_url }}' style="width: 100%" />
          </a>
        </p>
      </div>
    {% endif %}
  {% endfor %}
</div>

---
title: Code of Conduct
layout: home
permalink: /conduct/
---

# Code of Conduct

## TL;DR
- Treat everyone with respect and kindness.
- Be thoughtful in how you communicate.
- Don't be destructive or inflammatory.
- If you encounter an issue, please mail <a href='mailto&#58;co%6&#69;&#100;uc%74&#64;a&#37;&#54;6n&#111;%&#54;D&#46;net'>co&#110;duc&#116;&#64;a&#102;nom&#46;&#110;&#101;&#116;</a>

## Why do we have a Code of Conduct?

AFiniteNumberOfMonkeys’ community includes people from many different
backgrounds. We are committed to providing a friendly, safe, and welcoming
environment for all, regardless of age, disability, gender, nationality, race,
religion, sexuality, or similar personal characteristic.

The first goal of the Code of Conduct is to specify a baseline standard of
behaviour so that people with different social values and communication styles
can communicate effectively, productively, and respectfully.

The second goal is to provide a mechanism for resolving conflicts in the
community when they arise.

The third goal of the Code of Conduct is to make our community welcoming to
people from different backgrounds. Diversity is critical in order for us to
build a thriving community; for AFNOM to be successful, it needs hackers from
all backgrounds.

With that said, a healthy community must allow for disagreement and debate. The
Code of Conduct is not a mechanism for people to silence others with whom they
disagree. We value diverse opinions, but we value respectful behaviour more.

## Where does the Code of Conduct apply?

If you join in or contribute to the AFNOM ecosystem in any way, you are
encouraged to follow the Code of Conduct while doing so. By ‘member’ we
therefore refer to any person engaging with the society, in any capacity (e.g.
visitor, event attendee, Discord lurker, etc).

Explicit enforcement of the Code of Conduct applies to all official AFNOM
meetings and events, both in-person and online, including:

- The [Discord](https://afnom.net/discord) server
- The [Twitter](https://twitter.com/uob_afnom){:target="_new"} account
- The Club Meetings (hacking sessions)
- The Social Meetings (Pub)
- Any event (e.g. CTF, lightning talks), run in-person or on any platform, as
  necessary (e.g. Zoom, Jitsi, YouTube streams)
- The [GitLab](https://gitlab.com/afnom) projects

## AFNOM Values

These are the values to which people in the AFNOM community should aspire.

<h3 style="color: var(--accent)">Hacker Values </h3>
#### Be ethical
The single most important value when it comes to the object of our activities
is being ethical. Any illegal, unauthorised hacking is prohibited, and if we do
become aware of any of our members engaging in such behaviour we reserved the
right to remove them from our community without notice. 

All our community participants agree to adhere to all relevant local, national
and international laws governing computer misuse and hacking. Notably, [The
Computer Misuse Act
1990](https://www.legislation.gov.uk/ukpga/1990/18/contents){:target="_new"},
[Regulation of Investigatory Powers Act
2000](https://www.legislation.gov.uk/ukpga/2000/23/contents){:target="_new"}
and
[Serious Crime Act
2015](https://www.legislation.gov.uk/ukpga/2015/9/contents/enacted){:target="_new"}.
Our society’s spaces **are not** the place to brag about your illegal hacking of
friends/teachers/enemies.

#### Be responsible 
At AFNOM, we will provide you with safe, secure and isolated resources to
practice various offensive security techniques and tools. Remember, it is **up to
you** to exercise the knowledge and skills we share with you **responsibly** and
**lawfully**. AFNOM will not be taken responsible for any actions undertaken with
the tools or techniques explained during any of our society’s activities. 

Ethical Hacking is a fun and rewarding activity, but it is important to realise
the potentially devastating effects your actions may inadvertently have if
penetration testing skills are used
negligently.

<h3 style="color: var(--accent)"> Social Values</h3>
#### Be friendly and welcoming
#### Be patient
  * Remember that people have varying communication styles and that not
    everyone is using their native language (meaning and tone can be lost in
    translation). 
#### Be thoughtful
  * Productive communication requires effort. Think about how your words will
    be interpreted.
  * Remember that sometimes it is best to refrain entirely from commenting.
#### Be respectful
  * In particular, respect differences of opinion.
#### Be charitable
  * Interpret the arguments of others in good faith, do not seek to disagree.
  * When we do disagree, try to understand why.
#### Avoid destructive behaviour:
- Derailing: stay on topic; if you want to talk about something else, start a
  new conversation.
- Unconstructive criticism: don't merely condemn the current state of affairs;
  offer—or at least solicit—suggestions as to how things may be improved.
- Snarking (pithy, unproductive, sniping comments).
- Discussing potentially offensive or sensitive issues; this all too often
  leads to unnecessary conflict.
- Microaggressions: brief and commonplace verbal, behavioural, and environmental
  indignities that communicate hostile, derogatory or negative slights and
  insults to a person or group.


**People are complicated. You should expect to be misunderstood and to
misunderstand others; when this inevitably occurs, resist the urge to be
defensive or assign blame. Try not to take offence where no offence was
intended. Give people the benefit of the doubt. Even if the intent was to
provoke, do not rise to it. It is the responsibility of _all parties_ to
de-escalate conflict when it arises.**

## Unwelcome behaviour
These actions are explicitly forbidden in AFNOM spaces:

#### Expressing or provoking:
- Insulting, demeaning, hateful, or threatening remarks;
- Discrimination based on age, nationality, race, (dis)ability, gender
  (identity or expression), sexuality, religion, or similar personal
  characteristic;
- Bullying or systematic harassment;
- Unwelcome sexual advances, including sexually explicit content;
- Trolling, insulting/derogatory comments, and personal or political attacks.

#### Publishing others’ private information, such as a physical or electronic address, without explicit permission. 
#### Excessive advertisement for unnecessary or non-beneficial commercial products and services. 
#### Posting spam-like content that disrupts the environment of the community.

## Enforcement Responsibilities
Please understand that speech and actions have consequences, and unacceptable
behaviour will not be tolerated. When you participate in areas where the code of
conduct applies, you should act in the spirit of the "AFNOM values". If you
conduct yourself in a way that is explicitly forbidden by the Code of Conduct,
you will be warned and asked to stop, and your messages may be removed. As
AFNOM does not necessarily have a hierarchical structure, it is **all our
members’** responsibility to maintain our values and standards. Please be aware
that repeated offences may result in a temporary or permanent ban from the
community.

## Reporting and Resolving Issues
Instances of abusive, harassing, or otherwise unacceptable behaviour may be
reported to any of the `root` role members on Discord, or by sending an email
to <a
href='mailto&#58;co%6&#69;&#100;uc%74&#64;a&#37;&#54;6n&#111;%&#54;D&#46;net'>co&#110;duc&#116;&#64;a&#102;nom&#46;&#110;&#101;&#116;</a>.
All reports will be treated with confidentiality, in order to respect the privacy
and security of the reporter of any incident.

As we are an open community, we will strive to have an open discussion with all
our members about any issues arising, unless they are blatantly against our
Code of Conduct, and could be considered harmful to our members, in which case
the content will be removed without notice.

Note that the goal of our community is to resolve conflicts in the most
harmonious way possible. We hope that in most cases issues may be resolved
through polite discussion and mutual agreement. Bans and other forceful
measures are to be employed only as a last resort.

## Enforcement Guidelines
The following Community Impact Guidelines in determining the consequences for
any action they deem in violation of this Code of Conduct:

### 1. Correction
**Community Impact**: Use of inappropriate language or other behaviour deemed
unprofessional or unwelcome in the community.

**Consequence**: A private, written warning from community leaders, providing
clarity around the nature of the violation and an explanation of why the
behaviour was inappropriate. A public apology may be requested.

### 2. Warning
**Community Impact**: A violation through a single incident or series of actions.

**Consequence**: A warning with consequences for continued behaviour. No
interaction with the people involved, including unsolicited interaction with
those enforcing the Code of Conduct, for a specified period of time. This
includes avoiding interactions in community spaces as well as external channels
like social media. Violating these terms may lead to a temporary or permanent
ban.

### 3. Temporary Ban
**Community Impact**: A serious violation of community standards, including
sustained inappropriate behaviour.

**Consequence**: A temporary ban from any sort of interaction or public
communication with the community for a specified period of time. No public or
private interaction with the people involved, including unsolicited interaction
with those enforcing the Code of Conduct, is allowed during this period.
Violating these terms may lead to a permanent ban.

### 4. Permanent Ban
**Community Impact**: Demonstrating a pattern of violation of community standards,
including sustained inappropriate behaviour, harassment of an individual, or
aggression toward or disparagement of classes of individuals.

**Consequence**: A permanent ban from any sort of public interaction within the
community.

## Code of Conduct Changes
Changes to the AFNOM Code of Conduct should be proposed by creating an issue or
making a pull request to [this
document](https://gitlab.com/afnom/afnomnet/-/blob/master/conduct.md).

## Acknowledgements
This document was adapted from [Hacker Club’s Code of
Conduct](https://hackclub.com/conduct/){:target="_new"}. It was, in turn,
adapted from [Go's Code of
Conduct](https://golang.org/conduct){:target="_new"}, which is based on
[Contributor
Covenant](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html){:target="_new"},
the Code of Conduct for Open Source Projects. We also adapted some of the ideas
from [Sheffield Ethical Student Hackers Society’s Code of
Conduct](https://shefesh.com/downloads/SESH%20Code%20of%20Conduct.pdf){:target="_new"}.


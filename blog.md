---
title: Blog
layout: home
permalink: /blog/
---

## Posts

<div class="posts-list">
  <table class="posts-table">
    <thead>
      <td>Post</td>
      <td>By</td>
    </thead>

    {% for post in site.posts %}
      <tr>
        <td>
          <a href="{{ post.url | prepend: post.baseurl }}">{{ post.title }}</a>
        </td>
        <td>
          {{ post.author }}
        </td>
      </tr>
    {% endfor %}
  </table>
</div>

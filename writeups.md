---
title: Writeups
layout: home
permalink: /writeups/
---

## Write-ups

For submitting write-ups, please use [this template]("{{site.url}}/misc_files/template-writeup.md") if you want the file hosted on our site, or [the external template]("{{site.url}}/misc_files/external-writeup.md") if you plan to host the write-up on your own website. The format is Markdown. There are some explanations included in the files. Send them by email or submit a git pull request.

<div class="posts-list">
  <table class="posts-table">
    <thead>
      <td>Challenge</td>
      <td>By</td>
      <td>Competition</td>
    </thead>

    {% for writeup in site.writeups reversed %}
      <tr>
        <td>
          <a href="{% if writeup.ext-url %} {{ writeup.ext-url}} {% else %} {{ writeup.url | prepend: site.baseurl }} {% endif %}">
            {{ writeup.title }}
          </a>
        </td>
        <td>
          {{ writeup.author }}
        </td>
        <td>
          {{ writeup.ctf }}
        </td>
      </tr>
    {% endfor %}
  </table>
</div>
